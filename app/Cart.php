<?php

namespace App;

class Cart
{
    public $items = null;
    public $totalQty = 0;
    public $totalPrice = 0;
    public $coupon = 0;
    public $category = "";

    public function __construct($oldCart)
    {
        if($oldCart)
        {
            $this->items = $oldCart->items;
            $this->totalQty = $oldCart->totalQty;
            $this->totalPrice = $oldCart->totalPrice;
            $this->category = $oldCart->category;
            $this->coupon = $oldCart->coupon;
        }
    }

    public function add($item,$id,$cat,$qty)
    {
        $storedItem = ['qty' => $qty, 'price' => $item->price, 'item' => $item, 'category' => $cat];
        if($this->items)
        {
            if(array_key_exists($id,$this->items))
            {
                $storedItem = $this->items[$id];
                $storedItem['qty']+=$qty;
            }
        }
            $storedItem['price'] = $item->price * $storedItem['qty'];
            $this->items[$id] = $storedItem;
            $this->totalQty+=$qty;
            $this->totalPrice+=$item->price;
    }
    public function applyCoupon($coupon)
    {
        $this->coupon=$coupon;
    }
    public function getCouponValue()
    {
        return $this->totalPrice*($this->coupon/100);
    }
    public function getTaxValue()
    {
        return $this->totalPrice*(0.15);
    }
    public function getTotalPrice()
    {
        $total=$this->totalPrice+$this->getTaxValue()-$this->getCouponValue();
        return $total;
    }
    public function addOne($item,$id)
    {
        $tmp=$this->items[$id];
        if($this->items)
        {
            if(array_key_exists($id,$this->items))
            {
                $this->items[$id]['qty']++;
            }
        }
        $this->totalQty++;
        $this->totalPrice+=$item->price;
    }
    public function removeOne($item,$id)
    {
        $tmp=$this->items[$id];
        if($this->items)
        {
            if(array_key_exists($id,$this->items))
            {
                if($this->items[$id]['qty']>1)
                {
                    $this->items[$id]['qty']--;
                }
                else
                {
                     unset($this->items[$id]);
                }
            }
        }
            $this->totalQty--;
            $this->totalPrice-=$item->price;
    }
     public function remove($id)
    {
        $tmp=$this->items[$id];
        if($this->items)
        {
            if(array_key_exists($id,$this->items))
            {
                unset($this->items[$id]);
            }
        }
            $tmpPrice = $tmp['price'];
            $this->totalQty-= $tmp['qty'];
            $this->totalPrice-=$tmpPrice;
    }
}
