<?php

namespace App;

class Wishlist
{
    public $items = null;
    public $totalQty = 0;
    public $category = "";

    public function __construct($oldWishlist)
    {
        if($oldWishlist)
        {
            $this->items = $oldWishlist->items;
            $this->totalQty = $oldWishlist->totalQty;
            $this->category = $oldWishlist->category;
        }
    }

    public function add($item,$id,$cat)
    {
        $storedItem = ['qty' => 0, 'price' => $item->price, 'item' => $item, 'category' => $cat];
        if($this->items)
        {
            if(array_key_exists($id,$this->items))
            {
                $storedItem = $this->items[$id];
                $this->totalQty--;
            }
        }
            $this->totalQty++;
            $storedItem['price'] = $item->price;
            $this->items[$id] = $storedItem;
    }
     public function remove($id)
    {
        $tmp=$this->items[$id];
        if($this->items)
        {
            if(array_key_exists($id,$this->items))
            {
                unset($this->items[$id]);
            }
        }
            $this->totalQty--;
    }
}
