<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class visitor extends Model
{
    protected $fillable = [
         'email', 'message'
    ];
}
