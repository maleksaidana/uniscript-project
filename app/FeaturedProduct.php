<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeaturedProduct extends Model
{
    public function Product()
    {
        return $this->hasOne('App\Product','id','id');
    }
}
