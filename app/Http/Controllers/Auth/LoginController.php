<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Session;
use App\Cart;
use App\WishList;
use App\Product as Product;
use App\category as Category;
class LoginController extends Controller
{
    
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   
    public function __construct()
    {
            $this->middleware('guest')->except('logout');
            $this->middleware('guest:admin')->except('logout');
            $this->middleware('guest:employee')->except('logout');
    }
    public function webLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
            $user = Auth::user();
            $oldCart = Session::has('cart') ? Session::get('cart') : null;
            $oldWish = Session::has('Wishlist') ? Session::get('Wishlist') : null;
            $cart = new Cart($oldCart);
            $wish = new WishList($oldWish);
            $curent=DB::select('SELECT * FROM cart_perms WHERE iduser="'.$user->id.'"'); 
            $curent2=DB::select('SELECT * FROM wish_lists WHERE iduser="'.$user->id.'"'); 
            foreach($curent as $data){
               $Product = Product::find($data->idprod);
               $cat= Category::find($Product->idcat);
               $lang=Session::get('locale');
               if($lang=="fr"){
               $cart->add($Product,$Product->id,$cat->namefr,$data->qty);
               }
               else if($lang=="en"){
               $cart->add($Product,$Product->id,$cat->name,$data->qty);
               }
               else{
                $cart->add($Product,$Product->id,$cat->name,$data->qty);
               }
            }
            foreach($curent2 as $data){
               $Product = Product::find($data->idprod);
               $cat= Category::find($Product->idcat);
               $lang=Session::get('locale');
               if($lang=="fr"){
               $wish->add($Product,$Product->id,$cat->namefr);
               }
               else if($lang=="en"){
               $wish->add($Product,$Product->id,$cat->name);
               }
               else{
                $wish->add($Product,$Product->id,$cat->name);
               }
            }
            Session::put('cart',$cart);
            Session::put('Wishlist',$wish);
            $Cart=Session::get('cart');
            if($Cart!=null)
            {
                DB::table('cart_perms')->where('iduser', '=', $user->id)->delete();
                $cart = new Cart($Cart);
                $Products = $cart->items;
                if($Products!=null)
                {
                    foreach($Products as $Product) {
                        DB::table('cart_perms')->insert(
                        ['iduser' =>  $user->id, 'idprod' => $Product['item']['id'], 'qty' =>$Product['qty']]
                        );
                    }
                }
                else{
                    Session::forget('cart');
                }
            }
            $Wishlist=Session::get('Wishlist');
            if($Wishlist!=null)
            {
                DB::table('wish_lists')->where('iduser', '=', $user->id)->delete();
                $wish = new Wishlist($Wishlist);
                $Products = $wish->items;
                if($Products!=null)
                {
                    foreach($Products as $Product) {
                        DB::table('wish_lists')->insert(
                        ['iduser' =>  $user->id, 'idprod' => $Product['item']['id']]
                        );
                    }
                }
				else{
                    Session::forget('Wishlist');
                }
            }

            return redirect()->intended('/home');
        }
        return back()->withInput($request->only('email', 'remember'));
    }

    public function adminLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            return redirect()->intended('/home');
        }
        return back()->withInput($request->only('email', 'remember'));
    }


    public function employeeLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('employee')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            return redirect()->intended('/home');
        }
        return back()->withInput($request->only('email', 'remember'));
    }
}
