<?php

namespace App\Http\Controllers;
use App\category as Category;
use Illuminate\Support\Facades\DB;
use App\Product as Product;
use App\Discount as Discount;
use App\Comment as Comment;
use App\Sale as Sale;
use App\Rating as Rating;
use App\User as User;
use App\Employee as Employee;
use App\Cart_Perm as cart_perm;
use App\Reservation as Reservation;
use App\sale as sales;
use App\Cart;
use App\Wishlist;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\FeaturedProduct as FeaturedProduct;
use Session;
use Auth;

use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function bills()
	{
        if(Auth::guard('web')->check()){
           $id = Auth::guard('web')->user()->id;
         $sales=Sale::where('iduser', $id)->orderBy('saledate','desc')->get();
         return view('bills',["Sales"=>$sales]);
    }
    else{
        return redirect('/home');
    }
    }
    public function reservationclient()
	{
        if(Auth::guard('web')->check()){
           $id = Auth::guard('web')->user()->id;
         $sales=Reservation::where('iduser', $id)->orderBy('datereserved','desc')->get();
       
         return view('reservationclient',["Sales"=>$sales]);
    }
    else{
        return redirect('/home');
    }
    }    
       

    public function index()
	{
     
        $cat=Category::all();
        $prod=FeaturedProduct::all();
        
	 
		return view('index',["Categories"=>$cat])->with('Products',$prod);
	}


	public function Products(Request $req)
	{
		$name = $req->Product;
		$id = $req->cat;
		$order = $req->order;
		$pricefilter = $req->price;
		$ratingfilter = $req->rating;
        $priceOpr='>';
        $ratingOpr='>=';
        $price='0';
        $rating='0';
        if($pricefilter!='no-price')
        {
            $priceOpr='<';
            $price = $pricefilter;
        }
        if($ratingfilter!='no-rating')
        {
            $ratingOpr='>=';
            $rating = $ratingfilter;
        }
        $orderby="id";
        $way="ASC";
        if($order=="by-name")
        {
            $orderby="name";
        }
        elseif($order=="by-price-desc")
        {
            $orderby="price";
            $way="DESC";
        }
        elseif($order=="by-price-asc")
        {
            $orderby="price";
        }
        elseif($order=="by-rating-asc")
        {
            $orderby="stars";
            $way="ASC";
        }
        elseif($order=="by-rating-desc")
        {
            $orderby="stars";
            $way="DESC";
        }
        if($name!="" && $id!="0")
        {
            $prod =DB::table('products as p')
            ->select([

            'p.id',
            'p.name',
            'p.namefr',
            'p.descfr',
            'p.desc',
            'p.price',
            DB::raw( 'AVG(r.stars) as stars')])
            ->leftjoin('ratings as r','r.idprod', '=', 'p.id')
            ->Where([['price', $priceOpr,  $price],['idcat', '=', $id],['name', 'like', '%' . $name . '%'],[DB::raw( '(select avg(r.stars) from ratings r where r.idprod=p.id)'),$ratingOpr,$rating]])
            ->orWhere('desc', 'like', '%' . $name . '%')
            ->groupBy('p.id',
            'p.name',
            'p.namefr',
            'p.descfr',
            'p.desc',
            'p.price')
            ->orderBy($orderby,$way)
            ->paginate(6);
        }
        elseif($name=="" && $id!="0")
        {
            $prod =DB::table('products as p')
            ->select([
            'p.id',
            'p.name',
            'p.namefr',
            'p.descfr',
            'p.desc',
            'p.price',
            DB::raw( 'AVG(r.stars)  as stars')])
            ->leftjoin('ratings as r','r.idprod', '=', 'p.id')
            ->Where([['price', $priceOpr,  $price],['idcat', '=', $id],[DB::raw( '(select avg(r.stars) from ratings r where r.idprod=p.id)'),$ratingOpr,$rating]])
            ->groupBy('p.id',
            'p.name',
            'p.namefr',
            'p.descfr',
            'p.desc',
            'p.price')
            ->orderBy($orderby,$way)
            ->paginate(6);
        }
        elseif($name!="" && $id=="0")
        {
            $prod =DB::table('products as p')
            ->select([
            'p.id',
            'p.name',
            'p.namefr',
            'p.descfr',
            'p.desc',
            'p.price',
            DB::raw( 'AVG(r.stars) as stars')])
            ->leftjoin('ratings as r','r.idprod', '=', 'p.id')
            ->Where([['price', $priceOpr,  $price],['name', 'like', '%' . $name . '%'],[DB::raw( '(select avg(r.stars) from ratings r where r.idprod=p.id)'),$ratingOpr,$rating]])
            ->orWhere('desc', 'like', '%' . $name . '%')
            ->groupBy('p.id',
            'p.name',
            'p.namefr',
            'p.descfr',
            'p.desc',
            'p.price')
            ->orderBy($orderby,$way)
            ->paginate(6);
        } 
        else
        {
            $prod =DB::table('products as p')
            ->leftjoin('ratings as r','r.idprod', '=', 'p.id')
            ->select([DB::raw( 'avg(r.stars) as stars' ),
            'p.id',
            'p.name',
            'p.namefr',
            'p.descfr',
            'p.desc',
            'p.price'
            ])
            ->where(DB::raw( '(select avg(r.stars) from ratings r where r.idprod=p.id)'),$ratingOpr,$rating)
            ->Where('price', $priceOpr,  $price)
            ->groupBy('p.id',
            'p.name',
            'p.namefr',
            'p.descfr',
            'p.desc',
            'p.price')
            ->orderBy($orderby,$way)
            ->paginate(6);
        }
		return view('Products',["Products"=>$prod]);
	}

	public function Product_infos($id){
		$prod =Product::find($id);
        $Rating = Rating::where('idprod', $id)->avg('stars');
        $reviews = Rating::where('idprod', $id)->count();
        $Sales=Sale::Where([['idprod', '=', $id]])->count();
		return view('Product_infos',["Product"=>$prod,"Ratings"=>round($Rating),"Reviews"=>$reviews,"Sales"=>$Sales]);
	}


	public function Layoutcat()
    {
		
        $cat=Category::all()->toArray();
    
		
		return response()->json(['Categories' => $cat]);

    }


    public function SearchSuggestions(Request $request)
    {
        $lang=Session::get('locale');
	    if($request->ajax())
        {
            
            if($lang=='en'){
            $output="<tbody class='border'>";
            if($request->searchcat!="0")
            {
                
                    $Products=Product::Where([['idcat', '=', $request->searchcat],['name', 'like', '%' . $request->search . '%']])->orWhere([['idcat', '=', $request->searchcat],['desc', 'like', '%' . $request->search . '%']])->inRandomOrder()->limit(5)->get();                    
            }
            else
            {
              
              $Products=Product::Where('name','LIKE','%'.$request->search."%")->orWhere('desc', 'like', '%' . $request->search . '%')->inRandomOrder()->limit(5)->get();

              
            }
            if(!$Products->isEmpty())
            {
              
               foreach ($Products as $key => $Product) {

                    $output.='<tr onmousedown="search(\''.substr($Product->name, 0, 15).'\',\''.$Product->idcat.'\')"><td class="imagesearch text-center"><img src="'.asset('images/Products/'.$Product->id.'.jpg').'" alt=""></td><td class="Product"><strong>'.substr($Product->name, 0, 40).'...</strong></td></tr>';
                }
                
            }
            else
            {
                $output.="<tr><td class=Product text-muted text-center m-auto p-4>".__('home.found')."!</td></tr>";
            }
            $output.="</tbody>";
            
            return Response($output);
        }

        else if($lang=='fr'){
            $output="<tbody class='border'>";
            if($request->searchcat!="0")
            {
                
                    $Products=Product::Where([['idcat', '=', $request->searchcat],['namefr', 'like', '%' . $request->search . '%']])->orWhere([['idcat', '=', $request->searchcat],['descfr', 'like', '%' . $request->search . '%']])->inRandomOrder()->limit(5)->get();

                
                    
                
            }
            else
            {
              
              $Products=Product::Where('namefr','LIKE','%'.$request->search."%")->orWhere('descfr', 'like', '%' . $request->search . '%')->inRandomOrder()->limit(5)->get();

              
            }
            if(!$Products->isEmpty())
            {
              
            
               foreach ($Products as $key => $Product) {

                    $output.='<tr onmousedown="search(\''.substr($Product->name, 0, 15).'\',\''.$Product->idcat.'\')"><td class="imagesearch text-center"><img src="'.asset('images/Products/'.$Product->id.'.jpg').'" alt=""></td><td class="Product"><strong>'.substr($Product->namefr, 0, 40).'...</strong></td></tr>';
                  
                }
                
            }
            else
            {
                $output.="<tr><td class=Product text-muted text-center m-auto p-4>".__('home.found')."!</td></tr>";
            }
            $output.="</tbody>";
            
            return Response($output);
        }
        else{
            $output="<tbody class='border'>";
            if($request->searchcat!="0")
            {
                
                    $Products=Product::Where([['idcat', '=', $request->searchcat],['name', 'like', '%' . $request->search . '%']])->orWhere([['idcat', '=', $request->searchcat],['desc', 'like', '%' . $request->search . '%']])->inRandomOrder()->limit(5)->get();

                
                    
                
            }
            else
            {
              
              $Products=Product::Where('name','LIKE','%'.$request->search."%")->orWhere('desc', 'like', '%' . $request->search . '%')->inRandomOrder()->limit(5)->get();

              
            }
            if(!$Products->isEmpty())
            {
              
            
               foreach ($Products as $key => $Product) {

                    $output.='<tr onmousedown="search(\''.substr($Product->name, 0, 15).'\',\''.$Product->idcat.'\')"><td class="imagesearch text-center"><img src="'.asset('images/Products/'.$Product->id.'.jpg').'" alt=""></td><td class="Product"><strong>'.substr($Product->name, 0, 40).'...</strong></td></tr>';
                }
                
            }
            else
            {
                $output.="<tr><td class=Product text-muted text-center m-auto p-4>".__('home.found')."!</td></tr>";
            }
            $output.="</tbody>";
            
            return Response($output);

        } 
        }
        return null;
    }
    public function ClientSearchSuggestions(Request $request)
    {
        if($request->ajax())
        {
            $output="";
            $Clients=User::Where('firstname','LIKE','%'.$request->search."%")->orWhere('lastname','LIKE','%'.$request->search."%")->orWhere('email','LIKE','%'.$request->search."%")->inRandomOrder()->limit(5)->get();
            if(!$Clients->isEmpty())
            {
              
               foreach ($Clients as $key => $Client) {

                    $output.='<tr onmousedown="showuserdetails(\''.$Client->id.'\')"><td class="imagesearch text-center"><img src="'.asset('images/users/'.$Client->id.'.jpg').'" alt=""></td><td class="Product"><strong>'.$Client->lastname.' '.$Client->firstname.'</strong></td></tr>';
                }
                
            }
            else
            {
                $output.="<tr><td class=Product text-muted text-center m-auto p-4>".__('home.found')."!</td></tr>";
            }
            return Response($output);
        }
        return null;
    }
    public function ClientDetails(Request $request)
    {
        if($request->ajax())
        {
            $output="";
            $Clients=User::Where('id','=',$request->id)->get();
            if(!$Clients->isEmpty())
            {
              
               foreach ($Clients as $key => $Client) {
               $firstname=$Client->firstname;
               $lastname=$Client->lastname;
               $email=$Client->email;
               $statut=$Client->statut;
               $created_at=$Client->created_at->format('d M Y') ;
               $image="images/users/".$Client->id.".jpg";
                }
                
            }
            return response()->json(['firstname' => $firstname,'lastname' => $lastname,'email' => $email,'created_at' => $created_at,'statut' => $statut,'image' => $image]);
        }
        return null;
    }
    public function EmployeeSearchSuggestions(Request $request)
    {
        if($request->ajax())
        {
            $output="";
            $Employees=Employee::Where('firstname','LIKE','%'.$request->search."%")->orWhere('lastname','LIKE','%'.$request->search."%")->orWhere('email','LIKE','%'.$request->search."%")->inRandomOrder()->limit(5)->get();
            if(!$Employees->isEmpty())
            {
              
               foreach ($Employees as $key => $Employee) {

                    $output.='<tr onmousedown="showemployeedetails(\''.$Employee->id.'\')"><td class="imagesearch text-center"><img src="'.asset('images/employees/'.$Employee->id.'.jpg').'" alt=""></td><td class="Product"><strong>'.$Employee->lastname.' '.$Employee->firstname.'</strong></td></tr>';
                }
                
            }
            else
            {
                $output.="<tr><td class=Product text-muted text-center m-auto p-4>".__('home.found')."!</td></tr>";
            }
            return Response($output);
        }
        return null;
    }
    public function EmployeeDetails(Request $request)
    {
        if($request->ajax())
        {
            $output="";
            $Employees=Employee::Where('id','=',$request->id)->get();
            if(!$Employees->isEmpty())
            {
              
               foreach ($Employees as $key => $Employee) {
               $firstname=$Employee->firstname;
               $lastname=$Employee->lastname;
               $email=$Employee->email;
               $statut=$Employee->statut;
               $created_at=$Employee->created_at->format('d M Y') ;
               $image="images/employees/".$Employee->id.".jpg";
                }
                
            }
            return response()->json(['firstname' => $firstname,'lastname' => $lastname,'email' => $email,'created_at' => $created_at,'statut' => $statut,'image' => $image]);
        }
        return null;
    }
   public function getAddToCart(Request $request,$id)
    {
	   $Product = Product::find($id);
       $oldCart = Session::has('cart') ? Session::get('cart') : null;
       $cart = new Cart($oldCart);
       $cat= Category::find($Product->idcat);
$lang=Session::get('locale');
if($lang=="fr"){

       $cart->add($Product,$Product->id,$cat->namefr,1);}
       else if($lang=="en"){ $cart->add($Product,$Product->id,$cat->name,1);}
       else{
        $cart->add($Product,$Product->id,$cat->name,1);
       }

       $request->session()->put('cart',$cart);
       if($lang=="fr"){
       $output='<div class="row" onclick="window.location.href=\''.url("cart").'\'" style="cursor:pointer;"><div class="imagesearch col-3 text-center"><img src="'.asset('images/Products/'.$Product->id.'.jpg').'" class="rounded-circle notif-img" width="60" height="50" alt=""></div><div class="col-6 Product text-left"><strong> '.substr($Product->namefr, 0, 20).'...</strong></div><div class="col-3"><i class="fa fa-3x fa-plus-circle"></i></div></div>';}
       else if($lang=="en"){
        $output='<div class="row" onclick="window.location.href=\''.url("cart").'\'" style="cursor:pointer;"><div class="imagesearch col-3 text-center"><img src="'.asset('images/Products/'.$Product->id.'.jpg').'" class="rounded-circle notif-img" width="60" height="50" alt=""></div><div class="col-6 Product text-left"><strong> '.substr($Product->name, 0, 20).'...</strong></div><div class="col-3"><i class="fa fa-3x fa-plus-circle"></i></div></div>';}
       else{ $output='<div class="row" onclick="window.location.href=\''.url("cart").'\'" style="cursor:pointer;"><div class="imagesearch col-3 text-center"><img src="'.asset('images/Products/'.$Product->id.'.jpg').'" class="rounded-circle notif-img" width="60" height="50" alt=""></div><div class="col-6 Product text-left"><strong> '.substr($Product->name, 0, 20).'...</strong></div><div class="col-3"><i class="fa fa-3x fa-plus-circle"></i></div></div>';}
       return response()->json(['snackbar' => $output,'items' => $request->session()->get('cart')->totalQty]);
    }
    public function getRemoveFromCart(Request $request,$id)
    {
	   $Product = Product::find($id);
       $oldCart = Session::has('cart') ? Session::get('cart') : null;
       $cart = new Cart($oldCart);
       $cart->remove($Product->id);
       if($cart->totalQty==0){
          $request->session()->forget('cart');
       }
       else{
        $request->session()->put('cart',$cart);
       }
       return redirect()->action('PublicController@Cart');

    }
    public function getRemoveAllFromCart(Request $request)
    {
         $request->session()->forget('cart');
         if(Auth::guard('web')->check())
         {
              $user = Auth::user();
              DB::table('cart_perms')->where('iduser', '=', $user->id)->delete();
         }
         return redirect()->action('PublicController@Cart');
    }
    public function Cart()
    {
	   if(!Session::has('cart')){
        return view('cart',['Products' => null]);
       }
       $oldCart=Session::get('cart');
       $cart = new Cart($oldCart);

       return view('/cart',['Products' => $cart->items,'totalPrice' => $cart->totalPrice,'coupon' => $cart->coupon]);
    }
    public function addCoupon(Request $request,$coupon){
       $oldCart = Session::has('cart') ? Session::get('cart') : null;
       $cart = new Cart($oldCart);
       $discount = Discount::where('code', 'like', $coupon)->first();
       if($discount!=null)
       {
           $cart->applyCoupon($discount->discount);
           $request->session()->put('cart',$cart);
           $output='<i class="far fa-2x fa-check-circle text-success"></i>';
           $returnedSubtotal=sprintf('%0.2f',$cart->totalPrice);
           $returnedTax=sprintf('%0.2f',$cart->getTaxValue());
           $returnedCoupon=sprintf('%0.2f',$cart->getCouponValue());
           $returnedTotal=sprintf('%0.2f', $cart->getTotalPrice());
           return response()->json(['returned' => $output,'coupon' => $returnedCoupon,'tax' => '+'.$returnedTax.'$','sub' => $returnedSubtotal,'total' => $returnedTotal.'$']);
       }
        $output='<i class="far fa-2x fa-times-circle text-danger"></i>';
           $returnedCoupon=sprintf('%0.2f',$cart->getCouponValue());
           $returnedTotal=sprintf('%0.2f', $cart->getTotalPrice());
           return response()->json(['returned' => $output,'coupon' => $returnedCoupon,'total' => $returnedTotal.'$']);
    }
    public function addOne(Request $request,$id){
       $oldCart = Session::has('cart') ? Session::get('cart') : null;
       $cart = new Cart($oldCart);
       $Product = Product::find($id);
       $cart->addOne($Product,$Product->id);
       $request->session()->put('cart',$cart);
       $returnedSubtotal=sprintf('%0.2f',$cart->totalPrice);
       $returnedTax=sprintf('%0.2f',$cart->getTaxValue());
       $returnedCoupon=sprintf('%0.2f',$cart->getCouponValue());
       $returnedTotal=sprintf('%0.2f', $cart->getTotalPrice());
       return response()->json(['coupon' => $returnedCoupon,'subtotal' => $returnedSubtotal,'tax' => '+'.$returnedTax.'$','total' => $returnedTotal.'$']);
    }
    public function removeOne(Request $request,$id){
       $oldCart = Session::has('cart') ? Session::get('cart') : null;
       $cart = new Cart($oldCart);
       $Product = Product::find($id);
       $cart->removeOne($Product,$Product->id);
       if($cart->totalQty==0){
         $request->session()->forget('cart');
       }
       else{
         $request->session()->put('cart',$cart);
       }
       $returnedSubtotal=sprintf('%0.2f',$cart->totalPrice);
       $returnedTax=sprintf('%0.2f',$cart->getTaxValue());
       $returnedCoupon=sprintf('%0.2f',$cart->getCouponValue());
       $returnedTotal=sprintf('%0.2f', $cart->getTotalPrice());
       return response()->json(['coupon' => $returnedCoupon,'subtotal' => $returnedSubtotal,'tax' => '+'.$returnedTax.'$','total' => $returnedTotal.'$']);
    }
    public function getAddToWishlist(Request $request,$id)
    {$lang=Session::get('locale');
	   $Product = Product::find($id);
       $oldWishlist = Session::has('Wishlist') ? Session::get('Wishlist') : null;
       $Wishlist = new Wishlist($oldWishlist);
      
       $cat= Category::find($Product->idcat);
       if($lang=="fr"){
       $Wishlist->add($Product,$Product->id,$cat->namefr);}
       else if($lang=="en"){
        $Wishlist->add($Product,$Product->id,$cat->name);}
        else{
            $Wishlist->add($Product,$Product->id,$cat->name);}

       $request->session()->put('Wishlist',$Wishlist);
       if($lang=="fr"){
       $output='<div class="row" onclick="window.location.href=\''.url("Wishlist").'\'" style="cursor:pointer;"><div class="imagesearch col-3 text-center"><img src="'.asset('images/Products/'.$Product->id.'.jpg').'" class="rounded-circle notif-img" width="60" height="50" alt=""></div><div class="col-6 Product text-left"><strong> '.substr($Product->namefr, 0, 20).'...</strong></div><div class="col-3"><i class="fa fa-3x fa-star"></i></div></div>';}
       else if($lang=="en"){
        $output='<div class="row" onclick="window.location.href=\''.url("Wishlist").'\'" style="cursor:pointer;"><div class="imagesearch col-3 text-center"><img src="'.asset('images/Products/'.$Product->id.'.jpg').'" class="rounded-circle notif-img" width="60" height="50" alt=""></div><div class="col-6 Product text-left"><strong> '.substr($Product->name, 0, 20).'...</strong></div><div class="col-3"><i class="fa fa-3x fa-star"></i></div></div>';

       }
       else{
        $output='<div class="row" onclick="window.location.href=\''.url("Wishlist").'\'" style="cursor:pointer;"><div class="imagesearch col-3 text-center"><img src="'.asset('images/Products/'.$Product->id.'.jpg').'" class="rounded-circle notif-img" width="60" height="50" alt=""></div><div class="col-6 Product text-left"><strong> '.substr($Product->name, 0, 20).'...</strong></div><div class="col-3"><i class="fa fa-3x fa-star"></i></div></div>';
       }
       return response()->json(['snackbar' => $output,'items' => $request->session()->get('Wishlist')->totalQty]);
    }
    public function getRemoveFromWishlist(Request $request,$id)
    {
	   $Product = Product::find($id);
       $oldWishlist = Session::has('Wishlist') ? Session::get('Wishlist') : null;
       $Wishlist = new Wishlist($oldWishlist);
       $Wishlist->remove($Product->id);
       if($Wishlist->totalQty==0){
          $request->session()->forget('Wishlist');
       }
       else{
        $request->session()->put('Wishlist',$Wishlist);
       }
       return redirect()->action('PublicController@Wishlist');

    }
    public function getRemoveAllFromWishlist(Request $request)
    {
         $request->session()->forget('Wishlist');
         return redirect()->action('PublicController@Wishlist');
    }
    public function Wishlist()
    {
	   if(!Session::has('Wishlist')){
        return view('Wishlist',['Products' => null]);
       }
       $oldWishlist=Session::get('Wishlist');
       $Wishlist = new Wishlist($oldWishlist);

       return view('/Wishlist',['Products' => $Wishlist->items]);
    }
    public function getComments(Request $request,$id,$order)
    {
            $orderby="created_at";
            $way="DESC";
            if($order=="by-date-asc")
            {
                $orderby="created_at";
                $way="ASC";
            }
            elseif($order=="by-rating-asc")
            {
                $orderby="stars";
                $way="ASC";
            }
            elseif($order=="by-rating-desc")
            {
                $orderby="stars";
                $way="DESC";
            }
            $Comments =DB::table('comments as c')
            ->select([
            'c.idprod',
            'c.comment',
            'c.created_at',
            'c.iduser',
            'r.stars'])
            ->join('ratings as r', 'r.iduser', '=', 'c.iduser')
            ->where('c.idprod', '=', $id)
            ->where('r.idprod', '=', $id)
            ->groupBy('c.iduser', 'c.idprod', 'c.comment', 'c.created_at', 'r.stars')
            ->orderBy($orderby,$way)
            ->SimplePaginate(2);

            $output='';
            if($Comments!=null)
            {
                foreach ($Comments as $key => $Comment) {
                $User=User::find($Comment->iduser);
                $Sale=Sale::Where([['iduser', '=', $User->id],['idprod', '=', $id]])->first();
                $stars='';
                for ($x = 0; $x < $Comment->stars; $x++) {
                   $stars.='<i class="fa fa-star text-warning"></i>';
                }
                for ($x = 0; $x < (5-$Comment->stars); $x++) {
                   $stars.='<i class="fa fa-star text-muted"></i>';
                }
                $PurchaseState="";
                if($Sale!=null)
                {
                    $PurchaseState="<span class='badge text-success p-0'>".__('home.Purchase Verified')." <i class='far fa-check-circle text-success'></i></span>";
                }
                else
                {
                    $PurchaseState="<span class='badge text-muted p-0'>".__('home.Purchase Unconfirmed')." <i class='far fa-times-circle text-muted'></i></span>";
                }
                $output.='<li class="comment row">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-6 m-auto p-2">
                                <img src="'.asset('images/users/'.$User->id.'.jpg').'" class="rounded-circle" width="80" height="80">
                            </div>
                            <span class="comment-text col-10 m-auto"> 
							    <div class="p-2 row">
							        <div class="col-lg-9 col-md-9 col-sm-9 col-12 m-auto p-0">
							        <span class="text-dark" style="font-size:1.4em;"> '.  ucwords(strtolower($User->lastname)) .' '. ucwords(strtolower($User->firstname)).'</span>
								    <br>'.$PurchaseState.'
                                    </div>
								    <div class="col-lg-3 col-md-3 col-sm-3 col-6 m-auto p-0">
                                       <div class="py-3"> '.$stars.'</div>
								    </div>
                                    <p class="mt-2 mb-0 col-12 p-0">'.$Comment->comment.'</p>
							    </div> 
							</span>
                            
						  </li>
                          <span class="text-muted row text-right"><small class="col-10 p-0 m-auto float-right">'.\Carbon\Carbon::parse($Comment->created_at)->diffForHumans().'</small></span>
						<hr>';
                }
                $output.=$Comments->links();
            }
            else
            {
                $output.='<li class="Product text-muted text-center m-auto p-4">Be the first to comment!</li>';
            }
            return Response($output);
    }
    public function contact(Request $req){

       
        
        $email= $req->input('email');
        $message=$req->input('message');

        DB::insert('insert into visitor(email,message) values(?,?) ',[$email,$message]);
       
        Session::flash('message', 'Your message has been sent successfully ');
       return back();
    }

   
      
    
    public function updateuser(Request $req){
         if(Auth::guard('web')->check()){
        $decrypt="";

        $password=$req->input('password');
        $email=$req->input('email');
        $oldpass=$req->input('old');
        $curent=DB::select('SELECT password FROM users WHERE email="'.$email.'"');
        
        foreach($curent as $data){
            
                  $decrypt=$data->password;
            
        }
        

        $confirmp=$req->input('pass');
      
        $hash=Hash::make($password);
      
    if(password_verify($oldpass, $decrypt)){

        if($confirmp==$password){
        
        DB::table('users')
        ->where('email', $email)
        ->update(['password'=>$hash]);
        Session::flash('mensaje', 'Your password has been modified ');
       return back();
        }
        else{
            Session::flash('mensajer', 'Your password does not match '); 
            return back();
        }
    }
    else{
            Session::flash('merror', 'Your current password is not valid'
        ); 

            return back();}
    }
    else{
        return redirect('/home');
    }

        


    }
    public function updateemployee(Request $req){
        
      $decrypt="";
        $password=$req->input('password');
        $email=$req->input('email');
        $confirmp=$req->input('pass');
        $oldpass=$req->input('old');
        $curent=DB::select('SELECT password FROM employees WHERE email="'.$email.'"');
        
        foreach($curent as $data){
            
                  $decrypt=$data->password;
            
        }
        
        $hash=Hash::make($password);
if(password_verify($oldpass, $decrypt)){
        if($confirmp==$password){
        
         
            DB::table('employees')
            ->where('email', $email)
            ->update(['password'=>$hash]);
       
        Session::flash('mensaje', "Your password has been modified ");
        return back();}
        else{
            Session::flash('mensajer', "Your Password doesn't match");
            return back();}
        }
        else{
            Session::flash('merror', 'Your current password is not valid'
        ); 

            return back();}

        
        }



    public function updateadmin(Request $request){
        $decrypt="";
    

        $password=$request->input('password');
        $email=$request->input('email');
        $confirmp=$request->input('pass');
        $oldpass=$request->input('old');
        $curent=DB::select('SELECT password FROM admins WHERE email="'.$email.'"');
        
        foreach($curent as $data){
            
                  $decrypt=$data->password;
            
        }
        $hash=Hash::make($password);
if(password_verify($oldpass, $decrypt)){
        if($confirmp==$password){
        
         
            DB::table('admins')
            ->where('email', $email)
            ->update(['password'=>$hash]);
       
        Session::flash('mensaje', "Your password has been modified ");
        return back();}
        else{
            Session::flash('mensajer', "Your Password doesn't match");
            return back();
        }
    } else{
        Session::flash('merror', 'Your current password is not valid'); 

        return back();}
    }
    public function edituserinfo(Request $request) {
        if(Auth::guard('web')->check() || Auth::guard('employee')->check() || Auth::guard('admin')->check()){
        $nom= $request->input('firstname');
        $prenom=$request->input('lastname');
        $email=$request->input('email');
        DB::table('users')
        ->where('email', $email)
        ->update(['firstname' => $nom, 'lastname' => $prenom]);
   
    Session::flash('mens', "Your personal informations  has been modified ");
    return back();}
    else{
        return redirect('/home'); 
    }

    }
    public function postComment(Request $request)
    {
         if(Auth::guard('web')->check()){
         $user = Auth::user();
         $idprod=$request->id;
         $comment=$request->comment;
         $rating=$request->rating;
        DB::table('ratings')->insert(
                ['iduser' =>  $user->id, 'idprod' => $idprod, 'stars' =>$rating]
                );
        DB::table('comments')->insert(
            ['iduser' =>  $user->id, 'idprod' => $idprod, 'comment' =>$comment]
        );
        Session::flash('Commentres', "The comment has been posted ");
        return back();
        }
        else{
            return redirect('/home'); 
        }  
    }
    public function adduser(Request $request)
    {
         if(Auth::guard('admin')->check() || Auth::guard('employee')->check())
         {
            $firstname= $request->input('firstname');
            $lastname=$request->input('lastname');
            $email=$request->input('email');
            $birthdate=$request->input('birthdate');
            $password=$request->input('password');
            $hash=Hash::make($password);
        DB::table('users')->insert(
                ['firstname' => $firstname,'lastname' => $lastname,'email' => $email, 'birthdate' =>$birthdate,'statut' =>'user','password' =>$hash]
                );
        Session::flash('addres', "The user has been added ");
        return back();
        }
        else{
            return redirect('/home'); 
        }  
    }
    public function addemployee(Request $request)
    {
         if(Auth::guard('admin')->check())
         {
            $firstname= $request->input('firstname');
            $lastname=$request->input('lastname');
            $email=$request->input('email');
            $password=$request->input('password');
            $hash=Hash::make($password);
        DB::table('employees')->insert(
                ['firstname' => $firstname,'lastname' => $lastname,'email' => $email, 'statut' =>'employee','password' =>$hash]
                );
        Session::flash('addres', "The employee has been added ");
        return back();
        }
        else{
            return redirect('/home'); 
        }  
    }
    public function removeuser(Request $request)
    {
         if(Auth::guard('admin')->check()){
         $email=$request->input('emailD');
         DB::table('users')->where('email', '=', $email)->delete();
         Session::flash('mensd', "The user has been deleted ");
        return back();
        }
        else{
            return redirect('/home'); 
        }  
    }
    public function removeemployee(Request $request)
    {
         if(Auth::guard('admin')->check()){
         $email=$request->input('emailD');
         DB::table('employees')->where('email', '=', $email)->delete();
         Session::flash('mensd', "The employee has been deleted ");
        return back();
        }
        else{
            return redirect('/home'); 
        }  
    }
    public function editemployeeinfo(Request $request) {
        if(Auth::guard('admin')->check() || Auth::guard('employee')->check()){
        $nom= $request->input('firstname');
        $prenom=$request->input('lastname');
        $email=$request->input('email');
        DB::table('employees')
        ->where('email', $email)
        ->update(['firstname' => $nom, 'lastname' => $prenom]);
   
    Session::flash('mens', "Your personal informations has been modified ");
    return back();
    }
        else{
            return redirect('/home'); 
        }  
    }
    public function editadmininfo(Request $request) {
      if(Auth::guard('admin')->check()){
        $nom= $request->input('firstname');
        $prenom=$request->input('lastname');
        $email=$request->input('email');
        DB::table('admins')
        ->where('email', $email)
        ->update(['firstname' => $nom, 'lastname' => $prenom]);
   
    Session::flash('mens', "Your personal informations has been modified ");
    return back();}
        else{
            return redirect('/home'); 
        }  

    }

    public function reserve(Request $request){
        $output="";
        if(Auth::guard('web')->check()){
               $user = Auth::user();
               $instructions=$request->instructions;
               $discount=$request->discount;
              

                if(!Session::has('cart')){
                return view('cart',['Products' => null]);
               }
              
               $curent=DB::select('SELECT * FROM cart_perms WHERE iduser="'.$user->id.'"');
        
               foreach($curent as $data){
                        $prod =Product::find($data->idprod);
                        $subtotal=$prod->price*$data->qty;
                    DB::table('reservations')->insert(
                        ['iduser' =>  $user->id, 'idprod' => $data->idprod, 'qty' => $data->qty, 'instructions' =>$instructions, 'discount' =>$discount, 'subtotal' =>$subtotal,'state'=>0]
                    );
              
               }
              DB::table('cart_perms')->where('iduser', '=', $user->id)->delete();

               
               $request->session()->forget('cart');
               $output="reservation is completed";

        }
         return Response($output);
    }
    public function changeLanguage(Request $request)
    {
        $locale=$request->lang;
        $request->session()->put('locale',$locale);
        return redirect()->back();
    }
    public function stock(Request $req){
     
        $allprods=Product::all();
        $cat=Category::all();
            $prod =DB::table('products as p')
            ->select([

            'p.id',
            'p.name',
            'p.namefr',
            'p.descfr',
            'p.desc',
            'p.price',
            'p.quantity',
            DB::raw( 'AVG(r.stars)')])
            ->leftjoin('ratings as r','r.idprod', '=', 'p.id')
            ->groupBy('p.id',
            'p.name',
            'p.namefr',
            'p.descfr',
            'p.desc',
            'p.price',
            'p.quantity')
            ->paginate(6);
        
        
           
           
      
        
           
           
		return view('stock',["Products"=>$prod])->with('all',$allprods)->with('cat',$cat);
	



    }


    public function invoice(Request $request)
    {

        if(Auth::guard('admin')->check() || Auth::guard('employee')->check()){
      // $prod=DB::select("select a.iduser,a.saledate,sum(subtotal)as total,b.firstname,b.lastname  from sales a,users b where b.id=a.iduser  group by a.iduser,a.saledate,b.firstname,b.lastname order by saledate desc");
      $search=$request->search;
      $users=User::all();
      $prods=Product::all();
        if($search!=null){

        $prod=DB::table('sales as a')
        ->select([
        'a.iduser',
        'a.saledate',
        'b.firstname',
        'b.lastname',
        'a.discount',
         DB::raw('sum(a.subtotal) as total')])
         ->Join('users as b', 'b.id', '=', 'a.iduser')
         ->Where('b.email', 'like',  $search)
         ->groupBy('a.iduser','a.saledate','b.firstname','b.lastname','a.discount')
        ->orderByDesc('saledate')
        ->paginate(4);
        }
        else{
            $prod=DB::table('sales as a')
            ->select([
            'a.iduser',
            'a.saledate',
            'b.firstname',
            'b.lastname',
            'a.discount',
             DB::raw('sum(a.subtotal) as total')])
             ->Join('users as b', 'b.id', '=', 'a.iduser')
             ->groupBy('a.iduser','a.saledate','b.firstname','b.lastname','a.discount')
            ->orderByDesc('saledate')
            ->paginate(4);
            } 
        
        return view('invoice',["Sales"=>$prod])->with('prods',$prods)->with('users',$users);
        }
        else{
            return redirect('/home'); 
        }
    }
    public function stocksearch(Request $request){
        
        if(Auth::guard('admin')->check() || Auth::guard('employee')->check()){
            // $prod=DB::select("select a.iduser,a.saledate,sum(subtotal)as total,b.firstname,b.lastname  from sales a,users b where b.id=a.iduser  group by a.iduser,a.saledate,b.firstname,b.lastname order by saledate desc");
            $search=$request->search;
             $allprods=Product::all();
        $cat=Category::all();
              if($search!=null){
      
              $prod=DB::table('Products as a')
              ->select([
              'a.id',
              'a.name',
              'a.desc',
              'a.price',
              'a.quantity'])
              
              
               ->Where('a.name', 'like',  '%'.$search.'%')
               
              ->orderBy('id')
              ->paginate(4);
              }
              else{
                  $prod=DB::table('Products as a')
                  ->select([
                    'a.id',
                    'a.name',
                    'a.desc',
                    'a.price',
                    'a.quantity'])
                  
                  
                   ->Where('a.name', 'like',  '%'.$search.'%')
                  
                  ->orderBy('id')
                  ->paginate(4);
                  } 
              
                 
		return view('stock',["Products"=>$prod])->with('all',$allprods)->with('cat',$cat);
              }
              else{
                  return redirect('/home'); 
              }




    }

    
    public function bill_details(Request $request)
    {

    //  $prod=DB::select("select a.iduser,a.saledate,sum(subtotal)as total,b.firstname,b.lastname  from sales a,users b where b.id=a.iduser  group by a.iduser,a.saledate,b.firstname,b.lastname order by saledate desc");
       
    
    if(Auth::guard('admin')->check() || Auth::guard('employee')->check()){
    $client=User::where('id', $request->iduser)->first();
    
    $saledate=$request->saledate;
    $prod=DB::select("select a.id,b.name as idprod,a.subtotal,a.qty from sales a, products b where b.id=a.idprod and iduser='".$request->iduser."' and saledate='".$saledate."' ");
    $prod1=sales::where([['iduser', $request->iduser],['saledate',$saledate]])->first();
    $discount=$prod1->discount;


       return view('bill_details',["bill"=>$prod,"iduser"=>$client,"saledate"=>$saledate,"total"=>$request->total,"discount"=>$discount]);
    }
    else{
        return redirect('/home'); 
    }
    }
  

public function reservation(Request $request)
{

    if(Auth::guard('admin')->check() || Auth::guard('employee')->check() ){
  // $prod=DB::select("select a.iduser,a.saledate,sum(subtotal)as total,b.firstname,b.lastname  from sales a,users b where b.id=a.iduser  group by a.iduser,a.saledate,b.firstname,b.lastname order by saledate desc");
  $search=$request->search;
  
    if($search!=null){

    $prod=DB::table('reservations as a')
    ->select([
    'a.iduser',
    'a.datereserved',
    'b.firstname',
    'b.lastname',
    'a.discount',
     DB::raw('sum(a.subtotal) as total')])
     ->Join('users as b', 'b.id', '=', 'a.iduser')
     ->Where('b.email', 'like',  $search)
     ->groupBy('a.iduser','a.datereserved','b.firstname','b.lastname','a.discount')
    ->orderByDesc('datereserved')
    ->paginate(4);
    }
    else{
        $prod=DB::table('reservations as a')
        ->select([
        'a.iduser',
        'a.datereserved',
        'b.firstname',
        'b.lastname',
        'a.discount',
         DB::raw('sum(a.subtotal) as total')])
         ->Join('users as b', 'b.id', '=', 'a.iduser')
         ->groupBy('a.iduser','a.datereserved','b.firstname','b.lastname','a.discount')
        ->orderByDesc('datereserved')
        ->paginate(4);
        } 
    
    return view('reservation',["Sales"=>$prod]);
    }
    else{
        return redirect('/home'); 
    }
}


public function reservation_details(Request $request)
{

//  $prod=DB::select("select a.iduser,a.saledate,sum(subtotal)as total,b.firstname,b.lastname  from sales a,users b where b.id=a.iduser  group by a.iduser,a.saledate,b.firstname,b.lastname order by saledate desc");
   

if(Auth::guard('admin')->check() || Auth::guard('employee')->check()){
$client=User::where('id', $request->iduser)->first();

$saledate=$request->saledate;
$prod=DB::select("select a.id,b.name as idprod,a.subtotal,a.qty from reservations a, products b where b.id=a.idprod and iduser='".$request->iduser."' and datereserved='".$saledate."' ");
$prod1=reservation::where([['iduser', $request->iduser],['datereserved',$saledate]])->first();
$discount=$prod1->discount;

   return view('reservation_details',["bill"=>$prod,"iduser"=>$client,"saledate"=>$saledate,"total"=>$request->total,"discount"=>$discount]);
}
else{
    return redirect('/home'); 
}
}


public function check_reservation(Request $request)
{
    $iduser=$request->iduser;
    $reservationdate=$request->saledate;
    if(Auth::guard('admin')->check() || Auth::guard('employee')->check())
         {
                $curent=DB::select('SELECT * FROM reservations WHERE iduser="'.$iduser.'" and datereserved="'.$reservationdate.'"');
        
               foreach($curent as $data){
                    DB::table('sales')->insert(
                        ['iduser' =>  $data->iduser, 'idprod' => $data->idprod, 'qty' => $data->qty,'discount' =>$data->discount, 'subtotal' =>$data->subtotal]
                    );
              
               }
              DB::table('reservations')->where([['iduser', '=',  $iduser],['datereserved', '=', $reservationdate]])->delete();
         }



    return redirect('/reservation'); 
} 


public function delete_reservation(Request $request)
{
    
    $iduser=$request->iduser;
    $reservationdate=$request->saledate;
    if(Auth::guard('admin')->check() || Auth::guard('employee')->check())
         {
              DB::table('reservations')->where([['iduser', '=',  $iduser],['datereserved', '=', $reservationdate]])->delete();
         }
    return redirect('/reservation'); 
} 
public function delete_stock(Request $request)
{
    $id=$request->id;
   
   
    if(Auth::guard('admin')->check() )
         {
              DB::table('products')->where('id', '=',  $id)->delete();
         }
    return redirect('/stock'); 
}
public function addprod(Request $request)
{
     if(Auth::guard('admin')->check() )
     {
        $nameeng= $request->input('nameeng');
        $namefr=$request->input('namefr');
        $desceng=$request->input('desceng');
        $descfr=$request->input('descfr');
        $asin=$request->input('asin');
        $price=$request->input('price');
        $qty=$request->input('qty');
        $catid=$request->input('catid');


        DB::table('products')->insert(
            ['name' => $nameeng,'namefr' => $namefr,'descfr' => $descfr, 'desc' =>$desceng,'asin' =>$asin,'price' =>$price,'quantity' =>$qty,'idcat' =>$catid]
            );
    Session::flash('addres', "The product has been added ");
    return back();
    }
    else{
        return redirect('/home'); 
    }  
}
public function updateprod(Request $request)
{
     if(Auth::guard('admin')->check() )
     {
         $id= $request->input('id');
        $nameeng= $request->input('nameeng');
        $namefr=$request->input('namefr');
        $desceng=$request->input('desceng');
        $descfr=$request->input('descfr');
        $asin=$request->input('asin');
        $price=$request->input('price');
        $qty=$request->input('qty');
        $catid=$request->input('catid');

        if($nameeng!=null){
            DB::table('products')
            ->where('id', $id)
            ->update(['name'=>$nameeng]);
        }
        if($namefr!=null){
            DB::table('products')
            ->where('id', $id)
            ->update(['namefr'=>$namefr]);
        }
        if($desceng!=null){
            DB::table('products')
            ->where('id', $id)
            ->update(['desc'=>$desceng]);
        }
        if($descfr!=null){
            DB::table('products')
            ->where('id', $id)
            ->update(['descfr'=>$descfr]);
        }
        if($asin!=null){
            DB::table('products')
            ->where('id', $id)
            ->update(['asin'=>$asin]);
        }
        if($price!=null){
            DB::table('products')
            ->where('id', $id)
            ->update(['price'=>$price]);
        }
        if($qty!=null){
            DB::table('products')
            ->where('id', $id)
            ->update(['quantity'=>$qty]);
        }
        if($catid!=null){
            DB::table('products')
            ->where('id', $id)
            ->update(['idcat'=>$catid]);
        }

    Session::flash('addres', "The product has been updated ");
    return back();
    }
    else{
        return redirect('/home'); 
    }  
}
public function addinvoice(Request $request)
{
     if(Auth::guard('admin')->check() ||Auth::guard('employee')->check() )
     {
        $user= $request->input('user');
        $prod=$request->input('prod');
        $qty=$request->input('qty');
        $disc=$request->input('disc');
        $sub=$request->input('sub');


        DB::table('sales')->insert(
            ['iduser' => $user,'idprod' => $prod,'qty' => $qty, 'discount' =>$disc,'subtotal' =>$sub]
            );
    Session::flash('addres', "The invoice has been added ");
    return back();
    }
    else{
        return redirect('/home'); 
    }  
}
public function location(Request $request)
{

    if(Auth::guard('web')->check() || Auth::guard('admin')->check() || Auth::guard('employee')->check()){
  // $prod=DB::select("select a.iduser,a.saledate,sum(subtotal)as total,b.firstname,b.lastname  from sales a,users b where b.id=a.iduser  group by a.iduser,a.saledate,b.firstname,b.lastname order by saledate desc");
  $search=$request->search;
  $users=User::all();
  $prods=Product::all();
    if($search!=null){

    $prod=DB::table('sales as a')
    ->select([
    'a.iduser',
    'a.saledate',
    'b.firstname',
    'b.lastname',
    'a.discount',
     DB::raw('sum(a.subtotal) as total')])
     ->Join('users as b', 'b.id', '=', 'a.iduser')
     ->Where('b.email', 'like',  $search)
     ->groupBy('a.iduser','a.saledate','b.firstname','b.lastname','a.discount')
    ->orderByDesc('saledate')
    ->paginate(4);
    }
    else{
        $prod=DB::table('sales as a')
        ->select([
        'a.iduser',
        'a.saledate',
        'b.firstname',
        'b.lastname',
        'a.discount',
         DB::raw('sum(a.subtotal) as total')])
         ->Join('users as b', 'b.id', '=', 'a.iduser')
         ->groupBy('a.iduser','a.saledate','b.firstname','b.lastname','a.discount')
        ->orderByDesc('saledate')
        ->paginate(4);
        } 
    
    return view('location',["Sales"=>$prod])->with('prods',$prods)->with('users',$users);
    }
    else{
        return redirect('/home'); 
    }
}
public function addlocation(Request $request)
{
     if(Auth::guard('admin')->check() ||Auth::guard('employee')->check() )
     {
        $user= $request->input('user');
        $prod=$request->input('prod');
        $qty=$request->input('qty');
        $disc=$request->input('disc');
        $sub=$request->input('sub');


        DB::table('sales')->insert(
            ['iduser' => $user,'idprod' => $prod,'qty' => $qty, 'discount' =>$disc,'subtotal' =>$sub]
            );
    Session::flash('addres', "The Location has been added ");
    return back();
    }
    else{
        return redirect('/home'); 
    }  
}


}
