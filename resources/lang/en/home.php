<?php

return [
'home_p'=>'Home',
'about_p'=>'About Us',
'contact_p'=>'Contact Us',
'welcome_p'=>'Welcome to our store',
'log'=>' Log in',
'cancel'=>'Cancel',
'choose'=>'choose category',
'all'=>'All',
'trend'=>'Trending',
'prod'=>'products',
'search'=>'Search',
'member'=>'Member',
'all_cat'=>'All Categories',
'Gaming'=>'Gaming',
'Clothing & Fashion'=>'Clothing & Fashion',
'Computers & Tablets'=>'Computers & Tablets',
'PC Components'=>'PC Components',
'user'=>"username",
'pass'=>'password',
'your'=>'You are an employee',
'We are available on social media, follow us!'=>'We are available on social media, follow us!',

'team'=>'Our team',
'See our latest Products'=>'See our latest Products',
'see all our Products, their prices and discounts available easily from anywhere and anytime'=>'see all our Products, their prices and discounts available easily from anywhere and anytime',
'Our services'=>'Our services',
'ex'=>'our website provides you with all of our Products that are available in our store, 
  you can easily search for all the Products you are interested in
   and see their prices before and after taxes print
    them add them to your cart or even buy them directly. you can also view your purchases print the receipt of each of them',

'Your message has been sent succesfully'=>'Your message has been sent succesfully',

'enemail'=>'Enter your Email',
'reply'=>'We will reply to you by email',
'submit'=>'Submit',
'Address'=>'Adress',
'Email Address'=>"Email Address",
'Tel'=>'Phone',
'By price'=>'By price',
'Apply'=>'Apply',
'Filters'=>'Filters',
'Less than'=>'Less than',
'Result'=>'Result',
'By category'=>'By category',
'By rating'=>'By rating',
'Up'=>'Up',
'Order by'=>'Order by',
'found'=>'No results were found',
'None'=>'None',
'Price : high to low'=> 'Price : high to low',
'Price : low to high'=>'Price : low to high',
'Rating : low to high'=>'Rating : low to high',
'Rating : high to low'=>'Rating : high to low',
'Name'=>'Name',
'Product'=>'Product',
'Price'=>'Price',
'Remove'=>'Remove',
'Add'=>'Add',
'review(s)'=>'review(s)',
'order(s)'=> 'order(s)',
'tax'=>'tax',
'Add to cart'=>'Add to cart',
'Add to wishlist'=>'Add to wishlist',
'Reviews'=>'Reviews',
'Specifications'=>'Specifications',
'Product_des'=>'Product Description',
'Sales'=>'Sales',
'item(s) sold'=>'item(s) sold',
'Date first available at UniScript.ca'=>'Date first available at UniScript.ca',
'Ordered by Date : new to old'=>'Ordered by Date : new to old',
'Date : old to new'=>'Date : old to new',
'Date : new to old'=>'Date : new to old',
'for more options'=>'for more options',
'Submit Review'=>'Submit Review',
'Sign-in'=>'Sign-in',
'Are you a user ?'=>'Are you a user ?',
'Ordered by'=>'Ordered by',
'Purchase Verified'=>'Purchase Verified',
'Purchase Unconfirmed'=>'Purchase Unconfirmed',
'Show more'=>'Show more',
'Quantity'=>'Quantity',
'Clear Cart'=>'Clear Cart',
'If you have a coupon code, please enter it in the box below'=>'If you have a coupon code, please enter it in the box below',
'Instructions for our employees'=>'Instructions for our employees',
'If you have some information for the employees you can leave them in the box below'=>'If you have some information for the employees you can leave them in the box below',
'Coupons and additional discounts are shown below.You can only apply one at a time'=>'Coupons and additional discounts are shown below.You can only apply one at a time',
'Order summary'=>'Order summary',
'Coupons & discounts'=>'Coupons & discounts',
'Order Subtotal'=>'Order Subtotal',
'Reserve in store'=>'Reserve in store',
'Your shopping cart is'=>'Your shopping cart is',
'empty'=>'empty',
'Come back later'=>'Come back later',
'Apply coupon'=>'Apply coupon',
'Category'=>'Category',
'Your wishlist is'=>'Your wishlist is',
'Clear Wishlist'=>'Clear Wishlist',
'Are you sure'=>'Are you sure',


























];








?>
