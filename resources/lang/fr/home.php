<?php

return [
'home_p'=>'Accueil',
'about_p'=>'À propos',
'contact_p'=>'Contacter Nous',
'welcome_p'=>'Bienvenue chez notre magasin',
'log'=>' Connexion',
'cancel'=>' annuler',
'choose'=>'choisir la catégorie',
'all'=>'Tout',
'trend'=>'Tendance',
'search'=>'recherche',
'prod'=>'Produits',
'member'=>'Membre',
'all_cat'=>'toutes les catégories',
'Gaming'=>'Jeu',
'Clothing & Fashion'=>'Vêtements et mode',
'Computers & Tablets'=>'Ordinateurs et tablettes',
'PC Components'=>'Composants PC',
'user'=>"Nom d'utilisateur",
'pass'=>'mot de passe',
'your'=>'Vous êtes un employé',
'We are available on social media, follow us!'=>'On est disponible sur les réseaux sociaux!',
'team'=>'Notre équipe',
'See our latest Products'=>'Consulter nos derniers  produits ',
'see all our Products, their prices and discounts available easily from anywhere and anytime'=>'visionnez tous nos produits , leurs prix et leurs rabais sont disponibles tout le temps et partout',
'Our services'=>'Nos services',
'ex'=>"notre site web vous fournit tous nos produits disponibles dans notre magasin,
  vous pouvez facilement rechercher tous les produits qui vous intéressent
   et voyez leurs prix avant et après les taxes , les imprimez
    ,les ajouter à votre panier ou même les acheter directement. Vous pouvez également visualiser vos achats et  imprimer leurs factures",
    'Your message has been sent succesfully'=>'Votre message a été envoyé avec succès',
    'enemail'=>'Entrer votre e-mail',
    'reply'=>'On va vous répondre par e-mail',
    'submit'=>'Soumettre',
    'Email Address'=>"L'adresse électronique",
    'Address'=>"L'adresse",
    'Tel'=>'Tel',
    'By price'=>'Par prix',
    'Apply'=>'Appliquer',
    'Filters'=>'Filtres',
    'Less than'=>'Moins que',
    'Result'=>'Résultat',
    'By category'=>'Par catégorie ',
    'By rating'=>'Par évaluation',
    'Up'=>'Plus',
    'Order by'=>'Par ordre',
    'found'=>"aucun résultat n'est trouvé",
    'None'=>'Aucun',
    'Price : high to low'=>'Prix : du plus élevé au moins élevé',
    'Price : low to high'=>'Prix : du moins élevé au plus élevé',
    'Rating : low to high'=>'Évaluation : du moins élevé au plus élevé',
    'Rating : high to low'=>'Évaluation : du plus élevé au moins élevé',
    'Name'=>'Nom',
    'Product'=>'Produit',
    'Price'=>'Prix',
    'Remove'=>'Enlever',
    'Add'=>'Ajouter',
    'review(s)'=>'commentaire(s)',
    'order(s)'=>'commande(s)',
    'tax'=>'taxe',
    'Add to cart'=>'Ajouter au panier',
    'Add to wishlist'=>'Ajouter à la liste de souhaits',
    'Reviews'=>'Commentaires',
    'Specifications'=>'Spécifications',
    'Product_des'=>'Description des produits',
    'Sales'=>'Les ventes',
    'item(s) sold'=>'article(s) vendus',
    'Date first available at UniScript.ca'=>'La première date est disponible sur Uniscript.ca',
    'Ordered by Date : new to old'=>"Ordre par Date : de la courante à l'ancienne",
    'Date : old to new'=>" la Date : de l'ancienne à la courante",
    'Date : new to old'=>" la Date :de la courante à l'ancienne ",
    'for more options'=>"pour plus d'options",
    'Submit Review'=>'Soumettre le commentaire',
    'Sign-in'=>'Branchez-vous',
    'Are you a user ?'=>'Êtes-vous un(e) utilisateur(trice)',
    'Ordered by'=>"L'ordre par",
    'Purchase Verified'=>'Achat Vérifié',
    'Purchase Unconfirmed'=>'Achat Non Confirmé',
    'Show more'=>'montrer plus',
    'Quantity'=>'Quantité',
    'Clear Cart'=>'Vider le  panier',
    'If you have a coupon code, please enter it in the box below'=>'Si vous avez un code promo, veuillez le saisir dans la case ci-dessous',
'Instructions for our employees'=>'Les instructions pour les employés',
'If you have some information for the employees you can leave them in the box below'=>'Si vous avez des informations sur les employés, vous pouvez les laisser dans la case ci-dessous',

'Coupons and additional discounts are shown below.You can only apply one at a time'=>"
Les coupons et remises supplémentaires sont indiqués ci-dessous. Vous ne pouvez en appliquer qu'une à la fois",
'Order summary'=>'Sommaire de la commande',
'Coupons & discounts'=>'Coupons et rabais',
'Order Subtotal'=>'Sous-total de la commande',
'Reserve in store'=>'Réservez à notre magasin',
'Your shopping cart is'=>'Votre panier est ',
'empty'=>'vide',
'Come back later'=>'Retournez plus tard',
'Apply coupon'=>'Appliquer le coupon',
'Category'=>'La catégorie',
'Your wishlist is'=>'Ta liste de souhait est ',
'Clear Wishlist'=>'Vider ta liste de souhait',

'Are you sure'=>'Êtes-vous sûr',


















    







];








?>
