@extends('Layout')

@section('content')


<div class="container">
  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-body p-0">
                  <div class="row p-5">
                      <div class="col-md-6">
                          <img src="https://cdn.iconscout.com/icon/premium/png-64-thumb/payment-invoice-1725803-1466935.png">
                      </div>

                      <div class="col-md-6 text-right">
                          <p class="font-weight-bold mb-1">Reservation</p>
                          <p class="text-muted">Reservation Date: {{ $saledate }}</p>
                      </div>
                  </div>

                  <hr class="my-5">

                  <div class="row pb-5 p-5">
                      <div class="col-md-6">
                          <p class="font-weight-bold mb-4">Client Information</p>
                      <p class="mb-1">{{$iduser->firstname ." ".$iduser->lastname}}</p>
                          <p>Acme Inc</p>
                          <p class="mb-1">Montreal, Quebec</p>
                          <p class="mb-1"> Uniscript SexShop</p>
                      </div>

                      <div class="col-md-6 text-right">
                          <p class="font-weight-bold mb-4">Payment Details</p>
                          <p class="mb-1"><span class="text-muted">VAT: </span> Not Yet</p>
                          <p class="mb-1"><span class="text-muted">VAT ID: </span> Not Yet</p>
                          <p class="mb-1"><span class="text-muted">Payment Type: </span> Root</p>
                      <p class="mb-1"><span class="text-muted">Name: </span> {{$iduser->firstname  ." " .$iduser->lastname}}</p>
                      </div>
                  </div>

                  <div class="row p-5">
                      <div class="col-md-12">
                          <table class="table">
                              <thead>
                                  <tr>
                                      <th class="border-0 text-uppercase small font-weight-bold">ID</th>
                                      <th class="border-0 text-uppercase small font-weight-bold">Item</th>
                                      <th class="border-0 text-uppercase small font-weight-bold">Quantity</th>
                                       <th class="border-0 text-uppercase small font-weight-bold">Total</th>
                                  </tr>
                              </thead>
                              <tbody>
                                @foreach ($bill as $s)
                                  <tr>
                                      <td>{{$s->id}}</td>
                                      <td>{{$s->idprod}}</td>
                                      <td>{{$s->qty}}</td>
                                      <td>{{$s->subtotal}} $</td>
                                     
                                  </tr>
                                @endforeach
                               </tbody>
                          </table>
                      </div>
                  </div>

                  <div class="d-flex flex-row-reverse bg-dark text-white p-4">
                      <div class="py-3 px-5 text-right">
                          <div class="mb-2">Grand Total</div>
                          <div class="h2 font-weight-light">{{number_format((float)$total*1.15-$discount, 2, '.', '')}}$</div>
                      </div>

                      <div class="py-3 px-5 text-right">
                          <div class="mb-2">Discount</div>
                          <div class="h2 font-weight-light">-{{number_format((float)$discount, 2, '.', '')}}$</div>
                      </div>

                      <div class="py-3 px-5 text-right">
                          <div class="mb-2">Sub - Total amount</div>
                          <div class="h2 font-weight-light">{{number_format((float)$total, 2, '.', '')}}$</div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  
  <div class="text-light mt-5 mb-5 text-center small">by : <a class="text-light" target="_blank" href="http://totoprayogo.com">totoprayogo.com</a></div>

</div>

  
  @endsection
