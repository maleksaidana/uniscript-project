@extends('Layout')

@section('content')
@if(Auth::guard('admin')->check())
<div class="row p-0">
    <div class="col-md-4 col-11 m-auto p-2">
        <div class="row p-0">
            <form onsubmit="return false;" class="input-group m-auto col-10 p-0 d-flex" style="position:relative;">
                <input type="text" autocomplete="off" class="form-control employeesearchbox" name="Employees" placeholder="@lang('home.search')..."  >
            </form>
        </div>
        <div class="row p-0">
            <table class="table table-hover bg-white m-auto col-10 p-0">
                <tbody class='border' id="employeelivesearch">
                    
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-8 col-10 m-auto p-0">
    <div class="row">
      <div class="col mb-3">
        <div class="card">
          <div class="card-body">
                    <form class="form" method="post" action="{{action('PublicController@removeemployee')}}" >
                    @csrf
                        <input class="form-control emailD"  name="emailD"  type="hidden" value="example@example.com">
                        <button  class="btn btn-danger mx-2" type="submit"> <i class="fa fa-trash" style="font-size:1.4em;"></i></button>
                    </form>
                    <div class="row text-center">
                       <i class='fas fa-2x text-primary fa-plus-square btn m-auto col-12' data-toggle="collapse" data-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample2"> Add</i>
                       <div class="collapse col-md-6 col-12 m-auto" id="collapseExample2">
                          <div class="card card-body text-center">
                            <form class="form" method="post" action="{{action('PublicController@addemployee')}}">
                            @csrf
                                <div class="row">
                                  <div class="col">
                                    <div class="form-group">
                                      <label>First Name</label>
                                      <input class="form-control firstname" type="text" name="firstname" placeholder="firstname" value="">
                                    </div>
                                  </div>
                                  <div class="col">
                                    <div class="form-group">
                                      <label>Last Name</label>
                                      <input class="form-control lastname" type="text" name="lastname" placeholder="lastname" value="">
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col">
                                    <div class="form-group">
                                      <label>Email</label>
                                      <input class="form-control email"  name="email" type="text" placeholder="example@example.com">
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col">
                                    <div class="form-group">
                                      <label>Password</label>
                                      <input class="form-control password"  name="password" type="password" placeholder="••••••">
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                     <div class="col d-flex justify-content-start">
                                         <button  class="btn btn-primary" type="submit">Add employee</button>
                                     </div>
                                </div>
                                @if(Session::has('addres'))
                                  <p class="text-success">
                                       {{ Session::get('addres') }}
                                  </p>
                                @endif
                            </form>
                          </div>
                      </div>
                    </div>
            <div class="e-profile">
              <div class="row">
                <div class="col-12 col-sm-auto mb-3">
                  <div class="mx-auto" style="width: 140px;">
                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png" class="img-fluid rounded-circle image" alt="">
                  </div>
                </div>
                <div class="col d-flex flex-column flex-sm-row justify-content-between mb-3">
                  <div class="text-center text-sm-left mb-2 mb-sm-0">
                    <h4 class="pt-sm-2 pb-1 mb-0 text-nowrap firstnameT">firstname</h4>
                    <p class="mb-0 lastnameT">lastname</p>
                  </div>
                  <div class="text-center text-sm-right">
                    <span class="badge badge-secondary statut">statut</span>
                    <div class="text-muted"><small>Joined <span class="created_at">01 January 2000</span></small></div>
                  </div>
                </div>
              </div>
              <ul class="nav nav-tabs">
                <li class="nav-item"><a href="" class="active nav-link">Settings</a></li>
              </ul>
              <div class="tab-content pt-3">
                <div class="tab-pane active">
                    <form class="form" method="post" action="{{action('PublicController@editemployeeinfo')}}" >
                    @csrf
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <label>First Name</label>
                              <input class="form-control firstname" type="text" name="firstname" placeholder="firstname" value="">
                            </div>
                          </div>
                          <div class="col">
                            <div class="form-group">
                              <label>Last Name</label>
                              <input class="form-control lastname" type="text" name="lastname" placeholder="lastname" value="">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <label>Email</label>
                              <input disabled class="form-control email" type="text" value="example@example.com">
                              <input class="form-control emailH"  name="email"  type="hidden" value="example@example.com">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                             <div class="col d-flex justify-content-start">
                                 <button  class="btn btn-primary" type="submit">Save Changes</button>
                             </div>
                        </div>
                        @if(Session::has('mensd'))
                          <p class="text-danger">
                               {{ Session::get('mensd') }}
                          </p>
                        @endif
                        @if(Session::has('mens'))
                          <p class="text-success">
                               {{ Session::get('mens') }}
                          </p>
                        @endif
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
</div>
@endif


<script>


	$(document).ready(function(){
                
               
                $('.employeesearchbox').blur(function() {
                   $('#employeelivesearch').html("");
                });
                $('.employeesearchbox').focus(function() {
                   $('.employeelivesearch').keyup();
                });
                $('.employeesearchbox').keyup(delay(function (e) {
                    var minlength = 1;
                    var value=$(this).val();
                    if (value.length >= minlength ) {
                    $.ajax({
                    type : 'get',
                    url : '{{URL::to('EmployeeSearchSuggestions')}}',
                    data:{'search':value},
                    beforeSend: function(){
                        $('#employeelivesearch').html('<tbody class="border"><tr><td class="Product text-muted text-center m-auto p-4"><i class="fas fa-2x fa-spinner fa-spin"></i></td></tr></tbody>');
                    },
                    success:function(data){
                    $('#employeelivesearch').html(data);
                    }
                    });
                    }
                    else{
                        $('#employeelivesearch').html("");
                    }
			    },500));
			});
 function showemployeedetails(id)
 {
    $.ajax({
        type : 'get',
        url : '{{URL::to('EmployeeDetails')}}',
        data:{'id':id},
        beforeSend: function(){
        },
        success:function(data){
            $('.firstname').val(data.firstname);
            $('.lastname').val(data.lastname);
            $('.firstnameT').html(data.firstname);
            $('.lastnameT').html(data.lastname);
            $('.email').val(data.email);
            $('.emailH').val(data.email);
            $('.emailD').val(data.email);
            $('.statut').html(data.statut);
            $('.image').attr("src", data.image);
            $('.created_at').html(data.created_at);
        }
        });
 }
 function delay(callback, ms) {
  var timer = 0;
  return function() {
    var context = this, args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      callback.apply(context, args);
    }, ms || 0);
  };
}
</script>
@endsection
