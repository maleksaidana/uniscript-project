@extends('Layout')

@section('content')


<div class="container">
  <h2>Dashbord</h2>
  <div class="input-group col-lg-4 col-md-11 col-sm-11 col-11 m-auto p-0  d-flex justify-content-between">
   
    <input type="text" autocomplete="off" class="form-control search" id="search" name="search" placeholder="Search..."  >
    <span class="input-group-btn">
      <button class="btn btn-outline-info searchb" type="button"><i class="fas fa-search p-1"></i></button>
    </span>
                  <br/>
                  <table class="table table-hover bg-white" style="position: absolute;top: 100%;width: 100%;z-index:3" id="livesearch"></table>
  </div>                                                                                   
  <div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Name</th>
        <th>Date</th>
        <th>Total</th>
        <th> Check</th>
        <th> Accept </th>
        <th> Drop </th>
    </tr>
    </thead>
    <tbody>
      @foreach ($Sales as $s)
      <tr>
        <td>{{$s->iduser}}</td>
        <td>{{$s->firstname ." " .$s->lastname}} </td>
        <td>{{$s->datereserved}}</td>
        <td>{{number_format((float)$s->total*1.15-$s->discount, 2, '.', '')." $"}}</td>
        <td><a href="{{route('reservation_details', ['iduser'=>$s->iduser, 'saledate'=>$s->datereserved, 'total'=>$s->total]) }}" class="nav-link text-dark font-italic">
          <i class="fas fa-eye text-secondary mr-2" style="font-size:1.4em"></i>	
        </a></td>
        <td><a href="{{route('check_reservation', ['iduser'=>$s->iduser, 'saledate'=>$s->datereserved]) }}" class="nav-link text-dark font-italic">
          <i class="fas fa-check text-secondary mr-2" style="font-size:1.4em"></i>	
        </a></td>
        <td><a href="{{route('delete_reservation', ['iduser'=>$s->iduser, 'saledate'=>$s->datereserved]) }}" class="nav-link text-dark font-italic">
          <i class="fas fa-times text-secondary mr-2" style="font-size:1.4em"></i>	
        </a></td>
       </tr>
      @endforeach
    </tbody>
  </table>
  {{ $Sales->onEachSide(1)->links() }}
  </div>
</div>


<script>

 $(".searchb").click(function(){
  
  
  url="{{url('reservation/id')}}";
  url = url.replace('id',  $('#search').val());
  window.location.href = url;
});

</script>

  
  @endsection
