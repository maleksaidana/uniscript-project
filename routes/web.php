<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@index')->name('index');

Route::get('add-to-cart/{id}',[
    'uses' => 'PublicController@getAddToCart',
    'as' => 'Product.addToCart'
]);

Route::get('remove-from-cart/{id}',[
    'uses' => 'PublicController@getRemoveFromCart',
    'as' => 'Product.removeFromCart'
]);
Route::get('remove-all-from-cart',[
    'uses' => 'PublicController@getRemoveAllFromCart',
    'as' => 'Products.removeAllFromCart'
]);
Route::get('add-coupon/{coupon}',[
    'uses' => 'PublicController@addCoupon',
    'as' => 'Products.addCoupon'
]);
Route::get('add-one/{id}',[
    'uses' => 'PublicController@addOne',
    'as' => 'Products.addOne'
]);
Route::get('remove-one/{id}',[
    'uses' => 'PublicController@removeOne',
    'as' => 'Products.removeOne'
]);
Route::get('add-to-wishlist/{id}',[
    'uses' => 'PublicController@getAddToWishlist',
    'as' => 'Product.addToWishlist'
]);
Route::get('changeLanguage/{lang}',[
    'uses' => 'PublicController@changeLanguage',
    'as' => 'Product.changeLanguage'
]);
Route::get('remove-from-wishlist/{id}',[
    'uses' => 'PublicController@getRemoveFromWishlist',
    'as' => 'Product.removeFromWishlist'
]);
Route::get('remove-all-from-Wishlist',[
    'uses' => 'PublicController@getRemoveAllFromWishlist',
    'as' => 'Products.removeAllFromWishlist'
]);
Route::get('reserve/{instructions}/{discount}',[
    'uses' => 'PublicController@reserve',
    'as' => 'Products.reserve'
]);
Route::get('get-comments/{id}/{order}',[
    'uses' => 'PublicController@getComments',
    'as' => 'Product.getComments'
]);
Route::get('post-comment/{id}/{comment}/{rating}',[
    'uses' => 'PublicController@postComment',
    'as' => 'Product.postComment'
]);

Route::get('/Products/{cat?}/{order?}/{price?}/{rating?}/{Product?}', 'PublicController@Products')->name('Products');
/*Route::get('/products/{cat?}/{order?}/{Product?}', function () {
    return Redirect::to('/products/0/no-order');
});*/


Route::get('/Product_infos/{Product?}', 'PublicController@Product_infos')->name('Product_infos');

Route::get('/Layoutcat', 'PublicController@Layoutcat')->name('Layoutcat');

Route::get('/SearchSuggestions/{name?}/{id?}', 'PublicController@SearchSuggestions')->name('SearchSuggestions');
Route::get('/ClientSearchSuggestions/{name?}', 'PublicController@ClientSearchSuggestions')->name('ClientSearchSuggestions');
Route::get('/ClientDetails/{id?}', 'PublicController@ClientDetails')->name('ClientDetails');
Route::get('/EmployeeSearchSuggestions/{name?}', 'PublicController@EmployeeSearchSuggestions')->name('EmployeeSearchSuggestions');
Route::get('/EmployeeDetails/{id?}', 'PublicController@EmployeeDetails')->name('EmployeeDetails');


Route::get('/members', function () {
    return view('members');
});

Route::get('/employees', function () {
    return view('employees');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/contact', function () {
    return view('contact');
});
Route::get('/cart',[
     'uses' => 'PublicController@Cart',
    'as' => 'Product.cart'
]);
Route::get('/Wishlist',[
     'uses' => 'PublicController@Wishlist',
    'as' => 'Product.wishlist'
]);



Route::get('/cart', 'PublicController@Cart')->name('cart');
Route::get('/Wishlist', 'PublicController@Wishlist')->name('wishlist');
Route::post('contact','PublicController@contact');

Route::get('stock', 'PublicController@stock');

//Route::get('profile',function(){
   // return view('profile');
//});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::post('removeuser','PublicController@removeuser');
Route::post('removeemployee','PublicController@removeemployee');

Route::post('/login/employee', 'Auth\LoginController@employeeLogin');
Route::post('/login/admin', 'Auth\LoginController@adminLogin');
Route::post('/login', 'Auth\LoginController@webLogin');

Route::get('/profile', function () {
    if(Auth::guard('web')->check() || Auth::guard('admin')->check() || Auth::guard('employee')->check()){
    return view('profile');
    }
    else{
      return redirect()->route('home');
    }
});
Route::get('/bills', 'PublicController@bills')->name('bills');
Route::get('/reservationclient', 'PublicController@reservationclient')->name('reservationclient');
Route::get('updateuser','PublicController@updateuser');
Route::get('updateemployee','PublicController@updateemployee');
Route::get('updateadmin','PublicController@updateadmin');

Route::post('addprod','PublicController@addprod');
Route::post('updateprod','PublicController@updateprod');
Route::post('addinvoice','PublicController@addinvoice');
Route::post('adduser','PublicController@adduser');
Route::post('addemployee','PublicController@addemployee');

Route::post('edituserinfo','PublicController@edituserinfo');
Route::post('editemployeeinfo','PublicController@editemployeeinfo');
Route::get('editadmininfo','PublicController@editadmininfo');

/*malek*/
Route::get('invoice/{search?}','PublicController@invoice')->name('invoice');
Route::get('stocksearch/{search?}','PublicController@stocksearch')->name('stocksearch');

Route::get('/bill_details/{iduser?}/{saledate?}/{total?}', 'PublicController@bill_details')->name('bill_details');

Route::get('reservation/{search?}','PublicController@reservation')->name('reservation');
Route::get('location/{search?}','PublicController@location')->name('location');

Route::get('/reservation_details/{iduser?}/{saledate?}/{total?}', 'PublicController@reservation_details')->name('reservation_details');

Route::get('/check_reservation/{iduser?}/{saledate?}', 'PublicController@check_reservation')->name('check_reservation');
Route::get('/delete_reservation/{iduser?}/{saledate?}', 'PublicController@delete_reservation')->name('delete_reservation');
Route::get('/delete_stock/{idprod?}', 'PublicController@delete_stock')->name('delete_stock');
