<?php

use Illuminate\Database\Seeder;

class SalesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sales')->insert([
            [
            'id' => '1',
            'iduser' => '1',
            'idprod' => '1',
            'qty'=> '1',
            'discount'=> '0',
            'subtotal'=> '29.99'
            ],
            [
            'id' => '2',
            'iduser' => '2',
            'idprod' => '1',
            'qty'=> '1',
            'discount'=> '0',
            'subtotal'=> '29.99'
            ],
            [
            'id' => '3',
            'iduser' => '3',
            'idprod' => '1',
            'qty'=> '2',
            'discount'=> '0',
            'subtotal'=> '59.98'
            ],
            [
            'id' => '4',
            'iduser' => '4',
            'idprod' => '2',
            'qty'=> '1',
            'discount'=> '0',
            'subtotal'=> '19.99'
            ]

         ]);
    }
}
