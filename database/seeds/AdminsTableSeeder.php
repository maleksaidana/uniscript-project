<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            [
            'id' => '1',
            'firstname' => 'admin',
            'lastname' => 'admin',
            'email'=> 'admin@gmail.com',
            'statut'=>'administator',
            'password'=> '$2y$10$7iT08wYc1YcxP0Qat7opluAK/JqLpQFiGOrZ2qRz6OxTrAU6PiPSq'
            ]
                ]);
    }
}
