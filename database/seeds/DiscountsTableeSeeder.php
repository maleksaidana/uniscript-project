<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DiscountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('discounts')->insert([
            [
                'id' => '1',
                'discount' => '10',
                'code' => 'save10'
                
            ],
            [
                'id' => '2',
                'discount' => '20',
                'code' => 'save20'
            ],
            [
                'id' => '3',
                'discount' => '30',
                'code' => 'save10'
            ],
            [
                'id' => '4',
                'discount' => '40',
                'code' => 'save40'
            ],
            [
                'id' => '5',
                'discount' => '50',
                'code' => 'save50'
            ],
            [
                'id' => '6',
                'discount' => '60',
                'code' => 'save60'
            ],
            [
                'id' => '7',
                'discount' => '70',
                'code' => 'save70'
            ],
            [
                'id' => '8',
                'discount' => '80',
                'code' => 'save80'
            ]

    

                ]);
    }
}
