<?php

use Illuminate\Database\Seeder;

class RatingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ratings')->insert([
            [
            'id' => '1',
            'iduser' => '1',
            'idprod' => '1',
            'stars'=> '5'
            ],
            [
            'id' => '2',
            'iduser' => '2',
            'idprod' => '1',
            'stars'=> '4'
            ],
            [
            'id' => '3',
            'iduser' => '3',
            'idprod' => '1',
            'stars'=> '3'
            ],
            [
            'id' => '4',
            'iduser' => '4',
            'idprod' => '1',
            'stars'=> '1'
            ],
            [
            'id' => '5',
            'iduser' => '10',
            'idprod' => '1',
            'stars'=> '2.5'
            ],
            [
            'id' => '6',
            'iduser' => '10',
            'idprod' => '2',
            'stars'=> '2.5'
            ],
            [
            'id' => '7',
            'iduser' => '10',
            'idprod' => '3',
            'stars'=> '2.5'
            ],
            [
            'id' => '8',
            'iduser' => '10',
            'idprod' => '4',
            'stars'=> '2.5'
            ],
            [
            'id' => '9',
            'iduser' => '10',
            'idprod' => '5',
            'stars'=> '2.5'
            ],
            [
            'id' => '10',
            'iduser' => '10',
            'idprod' => '6',
            'stars'=> '2.5'
            ],
            [
            'id' => '11',
            'iduser' => '10',
            'idprod' => '7',
            'stars'=> '2.5'
            ],
            [
            'id' => '12',
            'iduser' => '10',
            'idprod' => '8',
            'stars'=> '2.5'
            ],
            [
            'id' => '13',
            'iduser' => '10',
            'idprod' => '9',
            'stars'=> '2.5'
            ],
            [
            'id' => '14',
            'iduser' => '10',
            'idprod' => '10',
            'stars'=> '2.5'
            ],
            [
            'id' => '15',
            'iduser' => '10',
            'idprod' => '11',
            'stars'=> '2.5'
            ]
         ]);
    }
}
