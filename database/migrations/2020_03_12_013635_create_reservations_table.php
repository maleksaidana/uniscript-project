<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger( 'iduser' )->unsigned();
            $table->foreign('iduser')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger( 'idprod' )->unsigned();
            $table->foreign('idprod')->references('id')->on('products')->onDelete('cascade');
            $table->string('instructions', 250)->collation('utf8_unicode_ci');
            $table->bigInteger('qty');
            $table->decimal('subtotal', 10, 2);
            $table->bigInteger( 'discount' )->unsigned();
            $table->boolean('state');
            $table->timestamp('datereserved')->useCurrent();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
