<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            
            $table->bigIncrements('id');
           // $table->string('code')->unique();
            $table->string('firstname')->collation('utf8_unicode_ci');
            $table->string('lastname')->collation('utf8_unicode_ci');
            $table->date('birthdate');
            $table->string('email')->unique();
          //  $table->char('status',1)->default('u');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('statut');
            $table->string('password');
            $table->rememberToken();
            $table->timestamp('created_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
